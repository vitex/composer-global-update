Composer Global Updater
=======================

![logo](composer-global-update.svg?raw=true)

Update PHP libraries in  /usr/share/* or /usr/lib/*


Usage
-----

Update all dependencies
``` bash
 $ composer-global-update
```

Update only dependencies with keyword within its name
``` bash
 $ composer-global-update nette
```

